package com.doranco.multitier.converter.service;

import java.math.BigDecimal;

import javax.ejb.Local;

@Local
public interface Converter {
	BigDecimal euroToyen (BigDecimal euros);
	BigDecimal euroToUSD (BigDecimal euros);

}
