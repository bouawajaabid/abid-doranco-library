package com.doranco.multitier.converter.serviceImpl;

import java.math.BigDecimal;

import javax.ejb.Stateless;

import com.doranco.multitier.converter.service.Converter;

@Stateless
public class ConverterBean implements Converter {
	
 private final BigDecimal tauxYen = new BigDecimal("129.823");
 private final BigDecimal tauxUSD = new BigDecimal("1.174");
 
 

public BigDecimal euroToyen(BigDecimal euros) {
	
	BigDecimal result = euros.multiply(tauxYen);
	
	return result.setScale(2,BigDecimal.ROUND_UP);
}

public BigDecimal euroToUSD(BigDecimal euros) {

BigDecimal result = euros.multiply(tauxUSD);
	return result.setScale(2, BigDecimal.ROUND_UP);
}
}
