import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;

public class StartClient {

	public static void main(String[] args)

	{

		Remote r;
		try {
			r = Naming.lookup("rmi://127.0.0.1/ServeurRMI");
			Information remoteObject = (Information) r;
			System.out.println(remoteObject.getInformation());
			System.out.println(remoteObject.sayHelloTo("Philippe"));
			System.out.println(remoteObject.doSomeAddition(25, 45));

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
