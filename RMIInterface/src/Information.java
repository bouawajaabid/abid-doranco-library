import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Information extends Remote {

	public String getInformation() throws RemoteException;

	public String sayHelloTo(String nom) throws RemoteException;

	public int doSomeAddition(int a, int b) throws RemoteException;
}