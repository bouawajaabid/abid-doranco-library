package com.doranco.rmi.chat;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IServer extends Remote{
   public IClient getClient(String username) throws RemoteException;
   public void login (IClient client) throws RemoteException;
   public void sendAll (String message, IClient from) throws RemoteException;
   
}
