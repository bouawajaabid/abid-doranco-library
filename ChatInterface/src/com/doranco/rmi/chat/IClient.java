package com.doranco.rmi.chat;

import java.rmi.Remote;
import java.rmi.RemoteException;


public interface IClient extends Remote{
 public String getUserName() throws RemoteException;
  public void tell(String message) throws RemoteException;
}
