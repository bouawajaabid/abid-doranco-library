package com.doranco.rmi.chat.serveur;

import java.net.Inet4Address;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

import com.doranco.rmi.chat.IClient;
import com.doranco.rmi.chat.IServer;

public class StartServer {

	public static void main(String[] args) {
		try {
	IServer serveur = new IServer() {
		
		@Override
		public void sendAll(String arg0, IClient arg1) throws RemoteException {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void login(IClient arg0) throws RemoteException {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public IClient getClient(String arg0) throws RemoteException {
			// TODO Auto-generated method stub
			return null;
		}
	};
	
		 LocateRegistry.createRegistry(1099);
		 String url="rmi://"+Inet4Address.getLocalHost().getHostAddress()+"/chatServeur";
				
			Naming.rebind(url, serveur);
			System.out.println("Enregistrement de l'objet avec url : " + url);
		}catch (RemoteException e) {
			e.printStackTrace();
		}catch (MalformedURLException e){
			e.printStackTrace();
		}catch (UnknownHostException e) {
			e.printStackTrace();
		}

	}
}
