package com.doranco.rmi.chat.serveur;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import com.doranco.rmi.chat.IClient;
import com.doranco.rmi.chat.IServer;

public class Serveur extends UnicastRemoteObject implements IServer {

	protected Serveur() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<IClient> clients = new ArrayList<IClient>();
	
	@Override
	public IClient getClient(String arg0) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void login(IClient client) throws RemoteException {
		// TODO Auto-generated method stub
		clients.add(client);
		client.tell("[SERVER] Bienvenue � toi "+client.getUserName());
		
	}

	@Override
	public void sendAll(String message, IClient from) throws RemoteException {
		// TODO Auto-generated method stub
		for (IClient client: clients) {
			if (!client.equals(from));
				client.tell(message);
		}
	}

}
