import java.net.Inet4Address;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class StartServeur {
	public static void main(String[] args) {
		try {
			LocateRegistry.createRegistry(1099);

			// instanciation de l'objet

			InformationImpl informationImpl = new InformationImpl();
			String url = "rmi://" + Inet4Address.getLocalHost().getHostAddress() + "/ServeurRMI";
			Naming.rebind(url, informationImpl);
			System.out.println("Enregistrement de l'objet avec url : " + url);

			System.out.println("Serveur lanc�");

		} catch (RemoteException e) {
			System.out.println("Probl�me d'acc�s � distance");
			e.printStackTrace();
		} catch (MalformedURLException e) {
			System.out.println("Probl�me avec URL enregistr�e");
			e.printStackTrace();
		} catch (UnknownHostException e) {
			System.out.println("H�te non reconnu");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Probl�me lors du d�marrage du serveur");
		}

	}

}
